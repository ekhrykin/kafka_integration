START bin\windows\zookeeper-server-start.bat config\zookeeper.properties
TIMEOUT 10
START bin\windows\kafka-server-start.bat config\server.properties
TIMEOUT 10
START bin\windows\kafka-run-class.bat kafka.admin.TopicCommand --delete --topic test --zookeeper localhost:2181
TIMEOUT 10
START bin\windows\kafka-console-consumer.bat --zookeeper localhost:2181 --topic test