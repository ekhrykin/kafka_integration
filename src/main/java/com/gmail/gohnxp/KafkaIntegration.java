package com.gmail.gohnxp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaIntegration {

    public static void main(String[] args) {
        SpringApplication.run(KafkaIntegration.class, args);
    }
}
