package com.gmail.gohnxp.config;

import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.gmail.gohnxp.service.ConsumerLoop;
import com.gmail.gohnxp.service.ServiceAggregator;

@Configuration
@ComponentScan(basePackages = { "com.gmail.gohnxp" })
public class SpringBootKafkaConsumer {

	private ConsumerLoop consumerLoop;
	
	@Autowired
	private ServiceAggregator serviceAggregator;
	
	@Value("${kafkaConsumerId}")
	private int kafkaConsumerId;
	
	@Value("${kafkaConsumersGroup}")
	private String kafkaConsumersGroup;
	
	@Value("${brokerList}")
	private String brokerList;

	@Value("${topic}")
	private String topic;

	@PostConstruct
	public void initIt() {			
		
		consumerLoop = new ConsumerLoop(kafkaConsumerId, 
							kafkaConsumersGroup,
							Arrays.asList(topic),
							brokerList, 
							serviceAggregator);
		consumerLoop.start();
	}
	
	@PreDestroy
	public void preDestroy(){
		consumerLoop.shutdown();
	}
}