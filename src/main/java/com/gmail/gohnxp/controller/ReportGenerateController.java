package com.gmail.gohnxp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.gmail.gohnxp.service.ReportsGeneratorService;

@Controller
//@RequestMapping("/api/invoice_info")
public class ReportGenerateController {

	private Logger logger = LoggerFactory.getLogger(ReportGenerateController.class);

	@Autowired
	ReportsGeneratorService reportsGeneratorService;

	private final static String SUCCESS_RESULT = "{result: 'success'}";
	private final static String FAIL_RESULT = "{result: 'fail'}";
	
//	@RequestMapping(value = "{invoiceInfoId}/reports", produces = "application/json", 
//			method = { RequestMethod.POST })
//	public @ResponseBody ResponseEntity<String> generateReport(@PathVariable("invoiceInfoId") int invoiceInfoId) {
//
//		return new ResponseEntity<>(SUCCESS_RESULT, HttpStatus.CREATED);
//	}
}
