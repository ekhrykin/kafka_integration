package com.gmail.gohnxp.domain;

public class MsgToProduce {

    private String name;
 
    public MsgToProduce(String name) {
        this.name = name;
    }
 
    public MsgToProduce() {
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
}
