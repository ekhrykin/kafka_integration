package com.gmail.gohnxp.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "RecordType")
public class RecordTypeEntity implements Serializable {

	private static final long serialVersionUID = -3023928433690475819L;

	@Id
	@Column(name = "RecordTypeId", nullable = false)
	private Integer id;

	@Column(name = "RecordTypeName", nullable = false)
	private String recordTypeName;

	@OneToMany(mappedBy = "recordType", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<RecordTypeFieldEntity> recordTypeFields;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRecordTypeName() {
		return recordTypeName;
	}

	public void setRecordTypeName(String recordTypeName) {
		this.recordTypeName = recordTypeName;
	}

	public List<RecordTypeFieldEntity> getRecordTypeFields() {
		return recordTypeFields;
	}

	public void setRecordTypeFields(List<RecordTypeFieldEntity> recordTypeFields) {
		this.recordTypeFields = recordTypeFields;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
