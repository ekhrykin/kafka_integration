package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RecordTypeFields")
public class RecordTypeFieldEntity implements Serializable {

	private static final long serialVersionUID = -3023928433690475820L;

	@Id
	@Column(name = "FieldId", nullable = false)
	private Integer id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RecordTypeId")
	private RecordTypeEntity recordType;

	@Column(name = "FieldName", nullable = false)
	private String fieldName;

	@Column(name = "DataType", nullable = false)
	private int dataType;

	@Column(name = "OrderNumInFieldGroup", nullable = false)
	private int order;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RecordTypeEntity getRecordType() {
		return recordType;
	}

	public void setRecordType(RecordTypeEntity recordType) {
		this.recordType = recordType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
