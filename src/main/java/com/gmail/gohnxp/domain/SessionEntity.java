package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SessionDefinitions")
public class SessionEntity implements Serializable {

	private static final long serialVersionUID = -3023928433690475813L;

	@Id
	@Column(name = "SessionID", nullable = false)
	private int id;

	@Column(name = "SessionDesc", nullable = true)
	private String description;

	@Column(name = "RecordTypeWhichCanInitiateSession", nullable = false)
	private int recordTypeWhichCanInitiateSession;

	@Column(name = "SessionStatsRecordType", nullable = false)
	private int sessionStatsRecordType;

	@Column(name = "PurgeTime", nullable = false)
	private int purgeTime;

	@Column(name = "HoldTime", nullable = false)
	private int holdTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHoldTime() {
		return holdTime;
	}

	public void setHoldTime(int holdTime) {
		this.holdTime = holdTime;
	}

	public int getPurgeTime() {
		return purgeTime;
	}

	public void setPurgeTime(int purgeTime) {
		this.purgeTime = purgeTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRecordTypeWhichCanInitiateSession() {
		return recordTypeWhichCanInitiateSession;
	}

	public void setRecordTypeWhichCanInitiateSession(int recordTypeWhichCanInitiateSession) {
		this.recordTypeWhichCanInitiateSession = recordTypeWhichCanInitiateSession;
	}

	public int getSessionStatsRecordType() {
		return sessionStatsRecordType;
	}

	public void setSessionStatsRecordType(int sessionStatsRecordType) {
		this.sessionStatsRecordType = sessionStatsRecordType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
