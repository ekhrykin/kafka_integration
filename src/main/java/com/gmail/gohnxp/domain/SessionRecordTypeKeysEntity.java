package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SessionRecordTypeKeys")
public class SessionRecordTypeKeysEntity implements Serializable {

	private static final long serialVersionUID = -3023928433690475818L;

	@Id
	@Column(name = "Keyid", nullable = false)
	private int id;

	@Column(name = "SessionId", nullable = false)
	private int sessionId;

	@Column(name = "RecordTypeId", nullable = false)
	private int recordTypeId;

	@Column(name = "KeysForMerge", nullable = false)
	private String keysForMerge;

	@Column(name = "KeysForPurge", nullable = true)
	private String keysForPurge;

	@Column(name = "ValueForPurge", nullable = true)
	private String valueForPurge;

	@Column(name = "DBIdForMerge", nullable = true)
	private String dbIdForMerge;

	@Column(name = "DBIdForPurge", nullable = true)
	private String dbIdForPurge;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public int getRecordTypeId() {
		return recordTypeId;
	}

	public void setRecordTypeId(int recordTypeId) {
		this.recordTypeId = recordTypeId;
	}

	public String getKeysForMerge() {
		return keysForMerge;
	}

	public void setKeysForMerge(String keysForMerge) {
		this.keysForMerge = keysForMerge;
	}

	public String getKeysForPurge() {
		return keysForPurge;
	}

	public void setKeysForPurge(String keysForPurge) {
		this.keysForPurge = keysForPurge;
	}

	public String getValueForPurge() {
		return valueForPurge;
	}

	public void setValueForPurge(String valueForPurge) {
		this.valueForPurge = valueForPurge;
	}

	public String getDbIdForMerge() {
		return dbIdForMerge;
	}

	public void setDbIdForMerge(String dbIdForMerge) {
		this.dbIdForMerge = dbIdForMerge;
	}

	public String getDbIdForPurge() {
		return dbIdForPurge;
	}

	public void setDbIdForPurge(String dbIdForPurge) {
		this.dbIdForPurge = dbIdForPurge;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
