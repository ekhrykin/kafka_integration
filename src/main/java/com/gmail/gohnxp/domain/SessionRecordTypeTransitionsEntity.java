package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SessionRecordTypeTransitions")
public class SessionRecordTypeTransitionsEntity implements Serializable {

	private static final long serialVersionUID = -3023928433690475813L;

	@Id
	@Column(name = "SessionRecordTypeId", nullable = false)
	private Integer sessionRecordTypeId;
	
	@Column(name = "SessionId", nullable = false)
	private int sessionId;
	
	@Column(name = "RecordTypeId", nullable = false)
	private int recordTypeId;

	@Column(name = "EndFlag", nullable = true)
	private Boolean endFlag;

	@Column(name = "endExpression", nullable = true)
	private String endExpression;

	@Column(name = "StatSavePropertiesId", nullable = true)
	private String statSavePropertiesId;

	@Column(name = "CorrespondingStatPropertyId", nullable = true)
	private String correspondingStatPropertyId;

	@Column(name = "MultipleOccuranceAllowed", nullable = true)
	private Boolean multipleOccuranceAllowed;
	
	
	

	public int getSessionRecordTypeId() {
		return sessionRecordTypeId;
	}

	public void setSessionRecordTypeId(int sessionRecordTypeId) {
		this.sessionRecordTypeId = sessionRecordTypeId;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public int getRecordTypeId() {
		return recordTypeId;
	}

	public void setRecordTypeId(int recordTypeId) {
		this.recordTypeId = recordTypeId;
	}

	public Boolean getEndFlag() {
		return endFlag;
	}

	public void setEndFlag(Boolean endFlag) {
		this.endFlag = endFlag;
	}

	public String getEndExpression() {
		return endExpression;
	}

	public void setEndExpression(String endExpression) {
		this.endExpression = endExpression;
	}

	public String getStatSavePropertiesId() {
		return statSavePropertiesId;
	}

	public void setStatSavePropertiesId(String statSavePropertiesId) {
		this.statSavePropertiesId = statSavePropertiesId;
	}

	public String getCorrespondingStatPropertyId() {
		return correspondingStatPropertyId;
	}

	public void setCorrespondingStatPropertyId(String correspondingStatPropertyId) {
		this.correspondingStatPropertyId = correspondingStatPropertyId;
	}

	public Boolean getMultipleOccuranceAllowed() {
		return multipleOccuranceAllowed;
	}

	public void setMultipleOccuranceAllowed(Boolean multipleOccuranceAllowed) {
		this.multipleOccuranceAllowed = multipleOccuranceAllowed;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
