package com.gmail.gohnxp.domain;

public enum StatObjectFieldKey {
	RECORD_TYPE_ID(1), VALUE(2), DATE_IN_MILLIS(3), SR4(4), SR5(5), SR6(6), SR7(7), SR8(8);

	private final int value;

	private StatObjectFieldKey(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public static StatObjectFieldKey getByValue(int value){
		StatObjectFieldKey result = null;
		for(StatObjectFieldKey enumObj : StatObjectFieldKey.values()){
			if(enumObj.value == value){
				result = enumObj;
				break;
			}
		}
		return result;
	}
}
