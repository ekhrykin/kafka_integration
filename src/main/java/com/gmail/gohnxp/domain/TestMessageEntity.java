package com.gmail.gohnxp.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TestMessage")
public class TestMessageEntity implements Serializable {

	private static final long serialVersionUID = -3023928433690475814L;

	@Id
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Column(name = "RecordTypeName", nullable = false)
	private String recordTypeName;
	
	@Column(name = "RecordTypeId", nullable = false)
	private int recordTypeId;

	@OneToMany(mappedBy = "testMessage", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<TestMessageFieldEntity> testMessageFields;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRecordTypeName() {
		return recordTypeName;
	}

	public void setRecordTypeName(String recordTypeName) {
		this.recordTypeName = recordTypeName;
	}

	public int getRecordTypeId() {
		return recordTypeId;
	}

	public void setRecordTypeId(int recordTypeId) {
		this.recordTypeId = recordTypeId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<TestMessageFieldEntity> getTestMessageFields() {
		return testMessageFields;
	}

	public void setTestMessageFields(List<TestMessageFieldEntity> testMessageFields) {
		this.testMessageFields = testMessageFields;
	}
}
