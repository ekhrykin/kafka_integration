package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TestMessageField")
public class TestMessageFieldEntity implements Serializable {

	private static final long serialVersionUID = -3023928433690475815L;

	@Id
	@Column(name = "ID", nullable = false)
	private Integer id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="TestMessageId")
	private TestMessageEntity testMessage;
	
	@Column(name = "FieldName", nullable = false)
	private String fieldName;
	
	@Column(name = "FieldId", nullable = false)
	private int fieldId;
	
	@Column(name = "LongValue", nullable = false)
	private Long longValue;
	
	@Column(name = "StrValue", nullable = false)
	private String strValue;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public int getFieldId() {
		return fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	public Long getLongValue() {
		return longValue;
	}

	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}

	public String getStrValue() {
		return strValue;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
