// automatically generated, do not modify

package com.gmail.gohnxp.domain.flatbuffers;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class KeyValuePair_int_ValueStore extends Table {
  public static KeyValuePair_int_ValueStore getRootAsKeyValuePair_int_ValueStore(ByteBuffer _bb) { return getRootAsKeyValuePair_int_ValueStore(_bb, new KeyValuePair_int_ValueStore()); }
  public static KeyValuePair_int_ValueStore getRootAsKeyValuePair_int_ValueStore(ByteBuffer _bb, KeyValuePair_int_ValueStore obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public KeyValuePair_int_ValueStore __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public int Key() { int o = __offset(4); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public ValueStore Value() { return Value(new ValueStore()); }
  public ValueStore Value(ValueStore obj) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static int createKeyValuePair_int_ValueStore(FlatBufferBuilder builder,
      int Key,
      int ValueOffset) {
    builder.startObject(2);
    KeyValuePair_int_ValueStore.addValue(builder, ValueOffset);
    KeyValuePair_int_ValueStore.addKey(builder, Key);
    return KeyValuePair_int_ValueStore.endKeyValuePair_int_ValueStore(builder);
  }

  public static void startKeyValuePair_int_ValueStore(FlatBufferBuilder builder) { builder.startObject(2); }
  public static void addKey(FlatBufferBuilder builder, int Key) { builder.addInt(0, Key, 0); }
  public static void addValue(FlatBufferBuilder builder, int ValueOffset) { builder.addOffset(1, ValueOffset, 0); }
  public static int endKeyValuePair_int_ValueStore(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

