// automatically generated, do not modify

package com.gmail.gohnxp.domain.flatbuffers;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class KeyValuePair_int_ValueStoreList extends Table {
  public static KeyValuePair_int_ValueStoreList getRootAsKeyValuePair_int_ValueStoreList(ByteBuffer _bb) { return getRootAsKeyValuePair_int_ValueStoreList(_bb, new KeyValuePair_int_ValueStoreList()); }
  public static KeyValuePair_int_ValueStoreList getRootAsKeyValuePair_int_ValueStoreList(ByteBuffer _bb, KeyValuePair_int_ValueStoreList obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public KeyValuePair_int_ValueStoreList __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public int id() { int o = __offset(4); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public KeyValuePair_int_ValueStore srcMem(int j) { return srcMem(new KeyValuePair_int_ValueStore(), j); }
  public KeyValuePair_int_ValueStore srcMem(KeyValuePair_int_ValueStore obj, int j) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int srcMemLength() { int o = __offset(6); return o != 0 ? __vector_len(o) : 0; }

  public static int createKeyValuePair_int_ValueStoreList(FlatBufferBuilder builder,
      int id,
      int srcMemOffset) {
    builder.startObject(2);
    KeyValuePair_int_ValueStoreList.addSrcMem(builder, srcMemOffset);
    KeyValuePair_int_ValueStoreList.addId(builder, id);
    return KeyValuePair_int_ValueStoreList.endKeyValuePair_int_ValueStoreList(builder);
  }

  public static void startKeyValuePair_int_ValueStoreList(FlatBufferBuilder builder) { builder.startObject(2); }
  public static void addId(FlatBufferBuilder builder, int id) { builder.addInt(0, id, 0); }
  public static void addSrcMem(FlatBufferBuilder builder, int srcMemOffset) { builder.addOffset(1, srcMemOffset, 0); }
  public static int createSrcMemVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startSrcMemVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static int endKeyValuePair_int_ValueStoreList(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
  public static void finishKeyValuePair_int_ValueStoreListBuffer(FlatBufferBuilder builder, int offset) { builder.finish(offset); }
};

