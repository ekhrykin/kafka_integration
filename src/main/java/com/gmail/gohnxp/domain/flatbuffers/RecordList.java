// automatically generated, do not modify

package com.gmail.gohnxp.domain.flatbuffers;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class RecordList extends Table {
  public static RecordList getRootAsRecordList(ByteBuffer _bb) { return getRootAsRecordList(_bb, new RecordList()); }
  public static RecordList getRootAsRecordList(ByteBuffer _bb, RecordList obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public RecordList __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long kafkaProducerRunningNum() { int o = __offset(4); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public KeyValuePair_int_ValueStoreList list(int j) { return list(new KeyValuePair_int_ValueStoreList(), j); }
  public KeyValuePair_int_ValueStoreList list(KeyValuePair_int_ValueStoreList obj, int j) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int listLength() { int o = __offset(6); return o != 0 ? __vector_len(o) : 0; }

  public static int createRecordList(FlatBufferBuilder builder,
      long kafkaProducerRunningNum,
      int listOffset) {
    builder.startObject(2);
    RecordList.addKafkaProducerRunningNum(builder, kafkaProducerRunningNum);
    RecordList.addList(builder, listOffset);
    return RecordList.endRecordList(builder);
  }

  public static void startRecordList(FlatBufferBuilder builder) { builder.startObject(2); }
  public static void addKafkaProducerRunningNum(FlatBufferBuilder builder, long kafkaProducerRunningNum) { builder.addLong(0, kafkaProducerRunningNum, 0); }
  public static void addList(FlatBufferBuilder builder, int listOffset) { builder.addOffset(1, listOffset, 0); }
  public static int createListVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startListVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static int endRecordList(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

