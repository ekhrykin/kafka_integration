// automatically generated, do not modify

package com.gmail.gohnxp.domain.flatbuffers;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class ValueStore extends Table {
  public static ValueStore getRootAsValueStore(ByteBuffer _bb) { return getRootAsValueStore(_bb, new ValueStore()); }
  public static ValueStore getRootAsValueStore(ByteBuffer _bb, ValueStore obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public ValueStore __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public int dataType() { int o = __offset(4); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public int id() { int o = __offset(6); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public double doubleValue() { int o = __offset(8); return o != 0 ? bb.getDouble(o + bb_pos) : 0; }
  public long longValue() { int o = __offset(10); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public int intValue() { int o = __offset(12); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean boolValue() { int o = __offset(14); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public int mCount() { int o = __offset(16); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public double doubleArray(int j) { int o = __offset(18); return o != 0 ? bb.getDouble(__vector(o) + j * 8) : 0; }
  public int doubleArrayLength() { int o = __offset(18); return o != 0 ? __vector_len(o) : 0; }
  public ByteBuffer doubleArrayAsByteBuffer() { return __vector_as_bytebuffer(18, 8); }
  public long longArray(int j) { int o = __offset(20); return o != 0 ? bb.getLong(__vector(o) + j * 8) : 0; }
  public int longArrayLength() { int o = __offset(20); return o != 0 ? __vector_len(o) : 0; }
  public ByteBuffer longArrayAsByteBuffer() { return __vector_as_bytebuffer(20, 8); }
  public String stringArray(int j) { int o = __offset(22); return o != 0 ? __string(__vector(o) + j * 4) : null; }
  public int stringArrayLength() { int o = __offset(22); return o != 0 ? __vector_len(o) : 0; }
  public String strValue() { int o = __offset(24); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer strValueAsByteBuffer() { return __vector_as_bytebuffer(24, 1); }

  public static int createValueStore(FlatBufferBuilder builder,
      int dataType,
      int id,
      double doubleValue,
      long longValue,
      int intValue,
      boolean boolValue,
      int mCount,
      int doubleArrayOffset,
      int longArrayOffset,
      int stringArrayOffset,
      int strValueOffset) {
    builder.startObject(11);
    ValueStore.addLongValue(builder, longValue);
    ValueStore.addDoubleValue(builder, doubleValue);
    ValueStore.addStrValue(builder, strValueOffset);
    ValueStore.addStringArray(builder, stringArrayOffset);
    ValueStore.addLongArray(builder, longArrayOffset);
    ValueStore.addDoubleArray(builder, doubleArrayOffset);
    ValueStore.addMCount(builder, mCount);
    ValueStore.addIntValue(builder, intValue);
    ValueStore.addId(builder, id);
    ValueStore.addDataType(builder, dataType);
    ValueStore.addBoolValue(builder, boolValue);
    return ValueStore.endValueStore(builder);
  }

  public static void startValueStore(FlatBufferBuilder builder) { builder.startObject(11); }
  public static void addDataType(FlatBufferBuilder builder, int dataType) { builder.addInt(0, dataType, 0); }
  public static void addId(FlatBufferBuilder builder, int id) { builder.addInt(1, id, 0); }
  public static void addDoubleValue(FlatBufferBuilder builder, double doubleValue) { builder.addDouble(2, doubleValue, 0); }
  public static void addLongValue(FlatBufferBuilder builder, long longValue) { builder.addLong(3, longValue, 0); }
  public static void addIntValue(FlatBufferBuilder builder, int intValue) { builder.addInt(4, intValue, 0); }
  public static void addBoolValue(FlatBufferBuilder builder, boolean boolValue) { builder.addBoolean(5, boolValue, false); }
  public static void addMCount(FlatBufferBuilder builder, int mCount) { builder.addInt(6, mCount, 0); }
  public static void addDoubleArray(FlatBufferBuilder builder, int doubleArrayOffset) { builder.addOffset(7, doubleArrayOffset, 0); }
  public static int createDoubleArrayVector(FlatBufferBuilder builder, double[] data) { builder.startVector(8, data.length, 8); for (int i = data.length - 1; i >= 0; i--) builder.addDouble(data[i]); return builder.endVector(); }
  public static void startDoubleArrayVector(FlatBufferBuilder builder, int numElems) { builder.startVector(8, numElems, 8); }
  public static void addLongArray(FlatBufferBuilder builder, int longArrayOffset) { builder.addOffset(8, longArrayOffset, 0); }
  public static int createLongArrayVector(FlatBufferBuilder builder, long[] data) { builder.startVector(8, data.length, 8); for (int i = data.length - 1; i >= 0; i--) builder.addLong(data[i]); return builder.endVector(); }
  public static void startLongArrayVector(FlatBufferBuilder builder, int numElems) { builder.startVector(8, numElems, 8); }
  public static void addStringArray(FlatBufferBuilder builder, int stringArrayOffset) { builder.addOffset(9, stringArrayOffset, 0); }
  public static int createStringArrayVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startStringArrayVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addStrValue(FlatBufferBuilder builder, int strValueOffset) { builder.addOffset(10, strValueOffset, 0); }
  public static int endValueStore(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

