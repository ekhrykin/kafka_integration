package com.gmail.gohnxp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gmail.gohnxp.domain.RecordTypeEntity;

public interface RecordTypeRepository 
	extends JpaRepository<RecordTypeEntity, Integer> {

}
