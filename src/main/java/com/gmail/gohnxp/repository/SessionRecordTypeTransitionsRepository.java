package com.gmail.gohnxp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gmail.gohnxp.domain.SessionRecordTypeTransitionsEntity;

@Repository
public interface SessionRecordTypeTransitionsRepository 
	extends JpaRepository<SessionRecordTypeTransitionsEntity, Integer> {

	SessionRecordTypeTransitionsEntity findOneBySessionIdAndRecordTypeId(int sessionId, int recordTypeId);
}
