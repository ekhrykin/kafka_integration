package com.gmail.gohnxp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gmail.gohnxp.domain.SessionRecordTypeKeysEntity;

@Repository
public interface SessionReportTypeKeysRepository 
	extends JpaRepository<SessionRecordTypeKeysEntity, Integer> {
	
	SessionRecordTypeKeysEntity findOneBySessionIdAndRecordTypeId(int sessionId, int recordTypeId);
}
