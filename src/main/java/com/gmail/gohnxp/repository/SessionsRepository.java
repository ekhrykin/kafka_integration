package com.gmail.gohnxp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gmail.gohnxp.domain.SessionEntity;

public interface SessionsRepository 
	extends JpaRepository<SessionEntity, Integer> {
	
	SessionEntity findOneByRecordTypeWhichCanInitiateSession(int recordTypeWhichCanInitiateSession);
	SessionEntity findOneById(int id);
	@Query(value = "select * from SessionRecordTypeTransitions t join "
			+ " SessionDefinitions s on t.SessionId = s.SessionID "
			+ " where t.RecordTypeId = :recordTypeId", nativeQuery = true)
	SessionEntity findSessionFromTransitionsByRecordTypeId(@Param("recordTypeId") int recordTypeId);
}
