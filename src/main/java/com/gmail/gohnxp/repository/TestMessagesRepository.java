package com.gmail.gohnxp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gmail.gohnxp.domain.TestMessageEntity;

public interface TestMessagesRepository 
	extends JpaRepository<TestMessageEntity, Integer> {

}
