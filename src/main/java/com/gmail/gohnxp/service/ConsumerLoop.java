package com.gmail.gohnxp.service;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.gohnxp.domain.SessionEntity;
import com.gmail.gohnxp.domain.SessionRecordTypeTransitionsEntity;
import com.gmail.gohnxp.domain.StatObjectFieldKey;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStoreList;

public class ConsumerLoop extends Thread {

	private final static Logger LOGGER = LoggerFactory.getLogger(ConsumerLoop.class);

	private final KafkaConsumer<String, byte[]> consumer;
	private final List<String> topics;
	private final int id;
	private ServiceAggregator serviceAggregator;

	public ConsumerLoop(int id, String groupId, List<String> topics, String brokerList,
			ServiceAggregator serviceAggregator) {
		this.id = id;
		this.topics = topics;
		this.serviceAggregator = serviceAggregator;

		Properties props = new Properties();
		props.put("bootstrap.servers", brokerList);
		props.put("group.id", groupId);
		props.put("key.deserializer", StringDeserializer.class.getName());
		props.put("value.deserializer", ByteArrayDeserializer.class.getName());
		this.consumer = new KafkaConsumer<>(props);
	}

	@Transactional
	public void processEventMsg(byte[] msgBytes) {
		serviceAggregator.getKvStoreService().write("TestKey".getBytes(), msgBytes);

		byte[] curMsgBytes = msgBytes;
		KeyValuePair_int_ValueStoreList fbMsg = KeyValuePair_int_ValueStoreList
				.getRootAsKeyValuePair_int_ValueStoreList(ByteBuffer.wrap(curMsgBytes));
		int recordTypeId = fbMsg.id();

		SessionEntity session = serviceAggregator.getSessionsRepository()
				.findOneByRecordTypeWhichCanInitiateSession(recordTypeId);
		if (session == null) {
			session = serviceAggregator
					.getSessionsRepository()
					.findSessionFromTransitionsByRecordTypeId(recordTypeId); //is it right? Can be only one unique session id?

			if(session == null){
				// TODO what to do?
				return;
			}
		}

		String mergeKey = FbStatObjectUtils.getMergeKey(fbMsg, session, serviceAggregator);
			
		if(mergeKey != null)
			processStatData(mergeKey, session, recordTypeId, curMsgBytes, fbMsg);

		String purgeKey = FbStatObjectUtils.getPurgeKey(fbMsg, session, serviceAggregator);
		String purgeValue = FbStatObjectUtils.getPurgeValue(fbMsg, session, serviceAggregator);
			
		if(purgeKey != null && purgeValue != null)
			serviceAggregator.getKvStoreService().write(purgeKey.getBytes(), 
					purgeValue.toString().getBytes()); //TODO what is a type of purge value?
	}

	private void processStatData(String mergeKey, SessionEntity session, int recordTypeId, byte[] curMsgBytes,
			KeyValuePair_int_ValueStoreList fbMsg){

		byte[] statObjectBytes = serviceAggregator.getKvStoreService().read((mergeKey + 
				FbStatObjectUtils.VALUE_PART_SEPARATOR + 0).getBytes());
		long curDate = new Date().getTime();

		if (statObjectBytes != null) {

			KeyValuePair_int_ValueStoreList fbStatObject = KeyValuePair_int_ValueStoreList
					.getRootAsKeyValuePair_int_ValueStoreList(ByteBuffer.wrap(statObjectBytes));
			long timeDeltaBetweenMessages = curDate - FbStatObjectUtils.getDate(fbStatObject);

			if (timeDeltaBetweenMessages < session.getHoldTime() * 1000 ) { //holdtime is in seconds
				int curStatObjectValue = FbStatObjectUtils.getValue(fbStatObject);

				SessionRecordTypeTransitionsEntity transistionsEntity = serviceAggregator
						.getSessionRecordTypeTransitionsRepository()
						.findOneBySessionIdAndRecordTypeId(session.getId(), recordTypeId);

				Map<StatObjectFieldKey, Object> propsForCopy = FbStatObjectUtils.getPropsForCopy(fbMsg,
						fbStatObject, session, serviceAggregator);

				serviceAggregator.getKvStoreService().write((mergeKey + 
						FbStatObjectUtils.VALUE_PART_SEPARATOR + 0).getBytes(),
						FbStatObjectUtils.create(session.getSessionStatsRecordType(),
								FbStatObjectUtils.getRecordTypeId(fbStatObject), curStatObjectValue + 1, curDate,
								propsForCopy));

				serviceAggregator.getKvStoreService().write((mergeKey + 
						FbStatObjectUtils.VALUE_PART_SEPARATOR + (curStatObjectValue + 1)).getBytes(),
						curMsgBytes);

				if (transistionsEntity.getEndFlag() != null && transistionsEntity.getEndFlag()) {
					// TODO what to do?
				}
			} else {
				// TODO process session data in upscaledb
			}
		} else {
			serviceAggregator.getKvStoreService().write((mergeKey + 
					FbStatObjectUtils.VALUE_PART_SEPARATOR + 0).getBytes(),
					FbStatObjectUtils.create(session.getSessionStatsRecordType(), recordTypeId, 1, curDate,
							Collections.<StatObjectFieldKey, Object> emptyMap()));

			serviceAggregator.getKvStoreService().write((mergeKey + 
					FbStatObjectUtils.VALUE_PART_SEPARATOR + 1).getBytes(),
					curMsgBytes);
		}		
	}
	
	@Override
	public void run() {
		try {
			consumer.subscribe(topics);
			while (true) {
				ConsumerRecords<String, byte[]> records = consumer.poll(Long.MAX_VALUE);
				for (ConsumerRecord<String, byte[]> record : records) {
					Map<String, Object> data = new HashMap<>();
					data.put("partition", record.partition());
					data.put("offset", record.offset());
					data.put("value", record.value());
					LOGGER.info("Received: " + this.id + ": " + data);

					processEventMsg(record.value());
				}
			}
		} catch (WakeupException e) {
			// ignore for shutdown
		} finally {
			consumer.close();
			LOGGER.info("Kafka consumer was shutdown.");
		}
	}

	public void shutdown() {
		consumer.wakeup();
	}
}