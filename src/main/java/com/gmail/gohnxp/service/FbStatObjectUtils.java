package com.gmail.gohnxp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gmail.gohnxp.domain.RecordTypeEntity;
import com.gmail.gohnxp.domain.RecordTypeFieldEntity;
import com.gmail.gohnxp.domain.SessionEntity;
import com.gmail.gohnxp.domain.SessionRecordTypeKeysEntity;
import com.gmail.gohnxp.domain.SessionRecordTypeTransitionsEntity;
import com.gmail.gohnxp.domain.StatObjectFieldKey;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStore;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStoreList;
import com.gmail.gohnxp.domain.flatbuffers.ValueStore;
import com.google.flatbuffers.FlatBufferBuilder;

public class FbStatObjectUtils {

	public static byte[] create(int keyForStatObject, int recordTypeId, int newStatObjectValue, long curDate,
			Map<StatObjectFieldKey, Object> propsForCopy) {

		FlatBufferBuilder builder = new FlatBufferBuilder();

		ValueStore.startValueStore(builder);
		ValueStore.addIntValue(builder, recordTypeId);
		int valueStoreRecordTypeIdOffset = ValueStore.endValueStore(builder);

		ValueStore.startValueStore(builder);
		ValueStore.addIntValue(builder, newStatObjectValue);
		int valueStoreIntOffset = ValueStore.endValueStore(builder);

		ValueStore.startValueStore(builder);
		ValueStore.addLongValue(builder, curDate);
		int valueStoreDateOffset = ValueStore.endValueStore(builder);

		// ===========================

		List<Integer> fieldValueOffsets = new ArrayList<>();

		KeyValuePair_int_ValueStore.startKeyValuePair_int_ValueStore(builder);
		KeyValuePair_int_ValueStore.addKey(builder, StatObjectFieldKey.RECORD_TYPE_ID.getValue());
		KeyValuePair_int_ValueStore.addValue(builder, valueStoreRecordTypeIdOffset);
		fieldValueOffsets.add(KeyValuePair_int_ValueStore.endKeyValuePair_int_ValueStore(builder));

		KeyValuePair_int_ValueStore.startKeyValuePair_int_ValueStore(builder);
		KeyValuePair_int_ValueStore.addKey(builder, StatObjectFieldKey.VALUE.getValue());
		KeyValuePair_int_ValueStore.addValue(builder, valueStoreIntOffset);
		fieldValueOffsets.add(KeyValuePair_int_ValueStore.endKeyValuePair_int_ValueStore(builder));

		KeyValuePair_int_ValueStore.startKeyValuePair_int_ValueStore(builder);
		KeyValuePair_int_ValueStore.addKey(builder, StatObjectFieldKey.DATE_IN_MILLIS.getValue());
		KeyValuePair_int_ValueStore.addValue(builder, valueStoreDateOffset);
		fieldValueOffsets.add(KeyValuePair_int_ValueStore.endKeyValuePair_int_ValueStore(builder));

		propsForCopy.forEach((key, value) -> {

			int strOffset = -1;
			if (value.getClass().equals(String.class))
				strOffset = builder.createString((String) value);

			ValueStore.startValueStore(builder);

			if (value.getClass().equals(String.class))
				ValueStore.addStrValue(builder, strOffset);
			else if (value.getClass().equals(Long.class))
				ValueStore.addLongValue(builder, (Long) value);
			else if (value.getClass().equals(Integer.class))
				ValueStore.addIntValue(builder, (Integer) value);
			else if (value.getClass().equals(Boolean.class))
				ValueStore.addBoolValue(builder, (Boolean) value);

			int fieldValueOffset = ValueStore.endValueStore(builder);

			//====
			
			KeyValuePair_int_ValueStore.startKeyValuePair_int_ValueStore(builder);
			KeyValuePair_int_ValueStore.addKey(builder, key.getValue());
			KeyValuePair_int_ValueStore.addValue(builder, fieldValueOffset);
			fieldValueOffsets.add(KeyValuePair_int_ValueStore.endKeyValuePair_int_ValueStore(builder));
		});

		// ===========================

		int[] srcMems = new int[fieldValueOffsets.size()];
		for (int i = 0; i < fieldValueOffsets.size(); i++)
			srcMems[i] = fieldValueOffsets.get(i);

		int srcMemVectorOffset = KeyValuePair_int_ValueStoreList.createSrcMemVector(builder, srcMems);

		KeyValuePair_int_ValueStoreList.startKeyValuePair_int_ValueStoreList(builder);
		KeyValuePair_int_ValueStoreList.addId(builder, keyForStatObject);
		KeyValuePair_int_ValueStoreList.addSrcMem(builder, srcMemVectorOffset);
		int intValueStoreListOffset = KeyValuePair_int_ValueStoreList.endKeyValuePair_int_ValueStoreList(builder);

		// ===========================

		// int[] list = {intValueStoreListOffset};
		// int listOffset = RecordList.createListVector(builder, list);
		//
		// RecordList.startRecordList(builder);
		// RecordList.addKafkaProducerRunningNum(builder,
		// KAFKA_PRODUCER_RUNNING_NUM);
		// RecordList.addList(builder, listOffset);
		// int recordListOffset = RecordList.endRecordList(builder);

		// ===========================

		builder.finish(intValueStoreListOffset);

		return builder.sizedByteArray();
	}

	public static long getDate(KeyValuePair_int_ValueStoreList statObject) {
		long result = -1;
		for (int i = 0; i < statObject.srcMemLength(); i++) {
			if (statObject.srcMem(i).Key() == StatObjectFieldKey.DATE_IN_MILLIS.getValue()) {
				result = statObject.srcMem(i).Value().longValue();
				break;
			}
		}
		return result;
	}

	public static int getValue(KeyValuePair_int_ValueStoreList statObject) {
		int result = -1;
		for (int i = 0; i < statObject.srcMemLength(); i++) {
			if (statObject.srcMem(i).Key() == StatObjectFieldKey.VALUE.getValue()) {
				result = statObject.srcMem(i).Value().intValue();
				break;
			}
		}
		return result;
	}

	public static int getRecordTypeId(KeyValuePair_int_ValueStoreList statObject) {
		int result = -1;
		for (int i = 0; i < statObject.srcMemLength(); i++) {
			if (statObject.srcMem(i).Key() == StatObjectFieldKey.RECORD_TYPE_ID.getValue()) {
				result = statObject.srcMem(i).Value().intValue();
				break;
			}
		}
		return result;
	}

	public static final String VALUE_PART_SEPARATOR = "\0";
	
	public static String getMergeKey(KeyValuePair_int_ValueStoreList fbMsg, SessionEntity session,
			ServiceAggregator serviceAggregator) {
		int recordTypeId = fbMsg.id();

		SessionRecordTypeKeysEntity sessionRecordTypeKeysEntity = serviceAggregator.getSessionReportTypeKeysRepository()
				.findOneBySessionIdAndRecordTypeId(session.getId(), recordTypeId);
		List<Integer> keys = Stream.of(sessionRecordTypeKeysEntity.getKeysForMerge().contains(",") ? 
					sessionRecordTypeKeysEntity.getKeysForMerge().split(",") : 
					sessionRecordTypeKeysEntity.getKeysForMerge().split("\\."))
				.map(keyStr -> Integer.valueOf(keyStr.trim())).collect(Collectors.toList());

		if(keys.size() == 0)
			return null;

		RecordTypeEntity recordType = serviceAggregator.getRecordTypeRepository().findOne(recordTypeId);

		String mergeKey = "";

		for (Integer key : keys)
			mergeKey += getFbMsgFieldValueByKey(key, fbMsg, recordType);
		
		return mergeKey;
	}

	public static String getPurgeKey(KeyValuePair_int_ValueStoreList fbMsg, SessionEntity session,
			ServiceAggregator serviceAggregator) {
		int recordTypeId = fbMsg.id();

		SessionRecordTypeKeysEntity sessionRecordTypeKeysEntity = serviceAggregator.getSessionReportTypeKeysRepository()
				.findOneBySessionIdAndRecordTypeId(session.getId(), recordTypeId);
		
		if(sessionRecordTypeKeysEntity.getKeysForPurge() == null)
			return null;
		
		List<Integer> keys = Stream.of(sessionRecordTypeKeysEntity.getKeysForPurge().contains(",") ? 
					sessionRecordTypeKeysEntity.getKeysForPurge().split(",") : 
					sessionRecordTypeKeysEntity.getKeysForPurge().split("\\."))
				.map(keyStr -> Integer.valueOf(keyStr.trim())).collect(Collectors.toList());

		if(keys.size() == 0)
			return null;

		RecordTypeEntity recordType = serviceAggregator.getRecordTypeRepository().findOne(recordTypeId);

		String purgeKey = "";

		for (Integer key : keys)
			purgeKey += (VALUE_PART_SEPARATOR + getFbMsgFieldValueByKey(key, fbMsg, recordType));
		
		return purgeKey.substring(VALUE_PART_SEPARATOR.length());
	}

	public static String getPurgeValue(KeyValuePair_int_ValueStoreList fbMsg, SessionEntity session,
			ServiceAggregator serviceAggregator) {
		int recordTypeId = fbMsg.id();

		SessionRecordTypeKeysEntity sessionRecordTypeKeysEntity = serviceAggregator.getSessionReportTypeKeysRepository()
				.findOneBySessionIdAndRecordTypeId(session.getId(), recordTypeId);
		
		if(sessionRecordTypeKeysEntity.getValueForPurge() == null)
			return null;
		
		List<Integer> keys = Stream.of(sessionRecordTypeKeysEntity.getValueForPurge().contains(",") ? 
					sessionRecordTypeKeysEntity.getValueForPurge().split(",") : 
					sessionRecordTypeKeysEntity.getValueForPurge().split("\\."))
				.map(keyStr -> Integer.valueOf(keyStr.trim())).collect(Collectors.toList());

		if(keys.size() == 0)
			return null;

		RecordTypeEntity recordType = serviceAggregator.getRecordTypeRepository().findOne(recordTypeId);

		String purgeValue = "";

		for (Integer key : keys)
			purgeValue += (VALUE_PART_SEPARATOR + getFbMsgFieldValueByKey(key, fbMsg, recordType));
		
		return purgeValue.substring(VALUE_PART_SEPARATOR.length());
	}

	public static Object getFbFieldValue(ValueStore fbValue, int valueTypeId) {
		Object result = null;

		switch (valueTypeId) {
		case 1:
			result = fbValue.intValue();
			break;
		case 2:
			result = fbValue.longValue();
			break;
		case 3:
			result = fbValue.strValue();
			break;
		case 5: // TODO what to do with missed date field type?
			result = fbValue.boolValue();
			break;
		case 6:
			result = fbValue.doubleValue();
			break;
		default:
			break;
		}

		return result;
	}

	public static Map<StatObjectFieldKey, Object> getPropsForCopy(KeyValuePair_int_ValueStoreList fbMsg,
			KeyValuePair_int_ValueStoreList fbStatObject, SessionEntity session, ServiceAggregator serviceAggregator) {

		Map<StatObjectFieldKey, Object> result = new HashMap<>();

		int recordTypeId = fbMsg.id();

		SessionRecordTypeTransitionsEntity transistionsEntity = serviceAggregator
				.getSessionRecordTypeTransitionsRepository()
				.findOneBySessionIdAndRecordTypeId(session.getId(), recordTypeId);

		if(transistionsEntity.getCorrespondingStatPropertyId() == null)
			return result;
		
		List<Integer> keysFrom = Stream.of(transistionsEntity.getCorrespondingStatPropertyId().split(","))
				.map(keyStr -> Integer.valueOf(keyStr.trim())).collect(Collectors.toList());

		List<Integer> keysTo = Stream.of(transistionsEntity.getStatSavePropertiesId().split(","))
				.map(keyStr -> Integer.valueOf(keyStr.trim())).collect(Collectors.toList());

		RecordTypeEntity recordType = serviceAggregator.getRecordTypeRepository().findOne(recordTypeId);

		for (int i = 0; i < keysFrom.size(); i++) {
			Integer keyFrom = keysFrom.get(i);
			Integer keyTo = keysTo.get(i);
			StatObjectFieldKey keyToEnumObject = StatObjectFieldKey.getByValue(keyTo);
			Object keyValue = getFbMsgFieldValueByKey(keyFrom, fbMsg, recordType);
			result.put(keyToEnumObject, keyValue);
		}

		return result;
	}

	private static Object getFbMsgFieldValueByKey(int key, KeyValuePair_int_ValueStoreList fbMsg,
			RecordTypeEntity recordType) {

		Object result = null;

		RecordTypeFieldEntity field = recordType.getRecordTypeFields().stream()
				.filter(aField -> aField.getOrder() == key).findFirst().get();

		for (int i = 0; i < fbMsg.srcMemLength(); i++) {
			KeyValuePair_int_ValueStore fbField = fbMsg.srcMem(i);
			if (fbField.Key() == field.getId()) {
				result = getFbFieldValue(fbField.Value(), field.getDataType());
				break;
			}
		}

		return result;
	}
}
