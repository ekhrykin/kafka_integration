package com.gmail.gohnxp.service;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.crupp.upscaledb.Database;
import de.crupp.upscaledb.DatabaseException;
import de.crupp.upscaledb.Environment;

@Service
public class KvStoreService {

	private final static Logger LOGGER = LoggerFactory.getLogger(KvStoreService.class);
	
    @Value("${upscaleDbFileName}")
    private String upscaleDbFileName;
    
    @Value("${upscaleDbNumber}")
    private short upscaleDbNumber;
    
    @Value("${spring.profiles.active}")
    private String springProfile;
    
	private final Environment env = new Environment();
	private Database db;
	
	public byte[] read(byte[] key){
		byte[] result = null;
		try {
			result = db.find(key);
		} catch (DatabaseException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return result;
	}
	
	public void write(byte[] key, byte[] value){
		
		try {
			if (read(key) != null)
				db.erase(key);
		} catch (DatabaseException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		try {
			db.insert(key, value);
		} catch (DatabaseException e1) {
			LOGGER.error(e1.getMessage(), e1);
		}
	}
	
	@PostConstruct
	public void init(){
		
		if(springProfile.equals("test") && new File(upscaleDbFileName).exists())
			new File(upscaleDbFileName).delete();
		
		try {
			if (new File(upscaleDbFileName).exists())
				env.open(upscaleDbFileName);
			else
				env.create(upscaleDbFileName);
		} catch (DatabaseException e) {
			LOGGER.error(e.getMessage(), e);
		}

		try {
			db = env.openDatabase(upscaleDbNumber, 0);
		} catch (DatabaseException e) {
			try {
				db = env.createDatabase(upscaleDbNumber, 0);
			} catch (DatabaseException e1) {
				LOGGER.error(e1.getMessage(), e1);
			}
		}
	}
	
	@PreDestroy
	public void preDestroy(){
		db.close();
		env.close();
	}
}
