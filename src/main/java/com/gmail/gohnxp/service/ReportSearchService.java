package com.gmail.gohnxp.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ReportSearchService {

	@Value("${reports.first_report.dest_file_name_path_with_prefix}")
	private String firstReportPathPrefix;

	@Value("${reports.second_report.dest_file_name_path_with_prefix}")
	private String secondReportPathPrefix;

	@Value("${reports.third_report.dest_file_name_path_with_prefix}")
	private String thirdReportPathPrefix;

	public byte[] search(int invoiceInfoId){
		return null;
	}
}
