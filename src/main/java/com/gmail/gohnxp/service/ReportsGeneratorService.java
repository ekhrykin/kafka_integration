package com.gmail.gohnxp.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ReportsGeneratorService {

	@Value("${reports.first_report.dest_file_name_path_with_prefix}")
	private String firstReportDestPathPrefix;

	@Value("${reports.second_report.dest_file_name_path_with_prefix}")
	private String secondReportDestPathPrefix;

	@Value("${reports.third_report.dest_file_name_path_with_prefix}")
	private String thirdReportDestPathPrefix;

}
