package com.gmail.gohnxp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gmail.gohnxp.repository.RecordTypeRepository;
import com.gmail.gohnxp.repository.SessionRecordTypeTransitionsRepository;
import com.gmail.gohnxp.repository.SessionReportTypeKeysRepository;
import com.gmail.gohnxp.repository.SessionsRepository;

@Service
public class ServiceAggregator {
	@Autowired
	private SessionsRepository sessionsRepository;
	@Autowired
	private SessionReportTypeKeysRepository sessionReportTypeKeysRepository;
	@Autowired
	private KvStoreService kvStoreService;
	@Autowired
	private RecordTypeRepository recordTypeRepository;
	@Autowired
	private SessionRecordTypeTransitionsRepository sessionRecordTypeTransitionsRepository;

	public SessionRecordTypeTransitionsRepository getSessionRecordTypeTransitionsRepository() {
		return sessionRecordTypeTransitionsRepository;
	}

	public void setSessionRecordTypeTransitionsRepository(
			SessionRecordTypeTransitionsRepository sessionRecordTypeTransitionsRepository) {
		this.sessionRecordTypeTransitionsRepository = sessionRecordTypeTransitionsRepository;
	}

	public RecordTypeRepository getRecordTypeRepository() {
		return recordTypeRepository;
	}

	public void setRecordTypeRepository(RecordTypeRepository recordTypeRepository) {
		this.recordTypeRepository = recordTypeRepository;
	}

	public SessionsRepository getSessionsRepository() {
		return sessionsRepository;
	}

	public void setSessionsRepository(SessionsRepository sessionsRepository) {
		this.sessionsRepository = sessionsRepository;
	}

	public SessionReportTypeKeysRepository getSessionReportTypeKeysRepository() {
		return sessionReportTypeKeysRepository;
	}

	public void setSessionReportTypeKeysRepository(SessionReportTypeKeysRepository sessionReportTypeKeysRepository) {
		this.sessionReportTypeKeysRepository = sessionReportTypeKeysRepository;
	}

	public KvStoreService getKvStoreService() {
		return kvStoreService;
	}

	public void setKvStoreService(KvStoreService kvStoreService) {
		this.kvStoreService = kvStoreService;
	}

}
