package com.gmail.gohnxp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gmail.gohnxp.domain.SessionRecordTypeTransitionsEntity;
import com.gmail.gohnxp.repository.SessionRecordTypeTransitionsRepository;

@Service
public class SessionRecordTypeTransitionsService {

	@Autowired
	SessionRecordTypeTransitionsRepository repo;
	
	public List<SessionRecordTypeTransitionsEntity> findAll() {
		return repo.findAll();
	}
}
