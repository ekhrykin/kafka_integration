package com.gmail.gohnxp.service;

import java.nio.ByteBuffer;

import org.springframework.stereotype.Service;

import com.gmail.gohnxp.domain.TestMessageEntity;
import com.gmail.gohnxp.domain.TestMessageFieldEntity;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStore;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStoreList;
import com.gmail.gohnxp.domain.flatbuffers.ValueStore;
import com.google.flatbuffers.FlatBufferBuilder;

@Service
public class TestMessageToFlatBufferConverter {

	public byte[] convert(TestMessageEntity testMessage){

    	FlatBufferBuilder builder = new FlatBufferBuilder();
    	
    	int[] fieldValuesOffsets = new int[testMessage.getTestMessageFields().size()];
    	
    	int fieldIndex = 0;
		for (TestMessageFieldEntity field : testMessage.getTestMessageFields()) {
			
			Object fieldValue = null;

			if(field.getLongValue() != null)
				fieldValue = field.getLongValue();
			else if(field.getStrValue() != null)
				fieldValue = builder.createString(ByteBuffer.wrap(field.getStrValue().getBytes()));
			
			ValueStore.startValueStore(builder);
			
			if(field.getLongValue() != null)
				ValueStore.addLongValue(builder, (Long) fieldValue);
			else if(field.getStrValue() != null)
				ValueStore.addStrValue(builder, (Integer) fieldValue);
			
			int valueStoreOffset = ValueStore.endValueStore(builder);
			fieldValuesOffsets[fieldIndex] = valueStoreOffset;
			fieldIndex++;
		}
    	
    	//===========================

		int[] fieldOffsets = new int[testMessage.getTestMessageFields().size()];
		fieldIndex = 0;
		for (TestMessageFieldEntity field : testMessage.getTestMessageFields()) {
			KeyValuePair_int_ValueStore.startKeyValuePair_int_ValueStore(builder);
			KeyValuePair_int_ValueStore.addKey(builder, field.getFieldId());
			KeyValuePair_int_ValueStore.addValue(builder, fieldValuesOffsets[fieldIndex]);
			int intValueStoreOffset = KeyValuePair_int_ValueStore.endKeyValuePair_int_ValueStore(builder);
			fieldOffsets[fieldIndex] = intValueStoreOffset;
			fieldIndex++;
		}
    	
    	//===========================
    	
    	int[] srcMems = fieldOffsets;
    	int srcMemVectorOffset = KeyValuePair_int_ValueStoreList.createSrcMemVector(builder, srcMems);

    	KeyValuePair_int_ValueStoreList.startKeyValuePair_int_ValueStoreList(builder);
    	KeyValuePair_int_ValueStoreList.addId(builder, testMessage.getRecordTypeId());    	
    	KeyValuePair_int_ValueStoreList.addSrcMem(builder, srcMemVectorOffset);
    	int intValueStoreListOffset = KeyValuePair_int_ValueStoreList.endKeyValuePair_int_ValueStoreList(builder);
    	
    	//===========================

//    	int[] list = {intValueStoreListOffset};
//    	int listOffset = RecordList.createListVector(builder, list);
//    	
//    	RecordList.startRecordList(builder);
//    	RecordList.addKafkaProducerRunningNum(builder, KAFKA_PRODUCER_RUNNING_NUM);    	
//    	RecordList.addList(builder, listOffset);
//    	int recordListOffset = RecordList.endRecordList(builder);
    	
    	//===========================

    	builder.finish(intValueStoreListOffset);
    	
    	return builder.sizedByteArray();
	}
}
