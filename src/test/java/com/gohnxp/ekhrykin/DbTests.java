package com.gohnxp.ekhrykin;

import java.io.File;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.gmail.gohnxp.KafkaIntegration;
import com.gmail.gohnxp.domain.RecordTypeEntity;
import com.gmail.gohnxp.domain.SessionEntity;
import com.gmail.gohnxp.domain.SessionRecordTypeKeysEntity;
import com.gmail.gohnxp.domain.SessionRecordTypeTransitionsEntity;
import com.gmail.gohnxp.domain.TestMessageEntity;
import com.gmail.gohnxp.repository.SessionReportTypeKeysRepository;
import com.gmail.gohnxp.repository.SessionsRepository;
import com.gmail.gohnxp.repository.TestMessagesRepository;
import com.gmail.gohnxp.service.ServiceAggregator;
import com.gmail.gohnxp.service.SessionRecordTypeTransitionsService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = KafkaIntegration.class)
@WebAppConfiguration
public class DbTests {

	@Autowired
	SessionRecordTypeTransitionsService transitionsService;

	@Autowired
	TestMessagesRepository testMessagesRepository;

	@Autowired
	ServiceAggregator serviceAggregator;

	@Value("upscaleDbFileName")
	String upscaleDbFileName;
	
	@Before
	public void init(){
		File aggDataDb = new File(upscaleDbFileName);
		if(aggDataDb.exists())
			aggDataDb.delete();
		
//		org.hsqldb.util.DatabaseManagerSwing.main(new String[] {
//				  "--url",  "jdbc:hsqldb:mem:testdb",
//				  "--user", "admin",
//				  "--password", "admin",
//				  "--noexit"
//				});
	}
	
	@Test
	public void testDbConnection() throws Exception {
		List<SessionRecordTypeTransitionsEntity> records = transitionsService.findAll();
		Assert.assertTrue("Something wrong with config db.", 
				records != null && 
				records.size() == 5 && 
				records.get(0).getRecordTypeId() == 10000000);
	}
	
	@Test
	public void testTestDataRetrive() throws Exception {
		List<TestMessageEntity> testMessages = testMessagesRepository.findAll();
		
		Assert.assertTrue("Can't read test messages.", 
				testMessages != null && testMessages.size() == 5 && 
				testMessages.get(0).getRecordTypeName().equals("TestR1"));
		
		Assert.assertTrue("Can't read test message fields.",
				!testMessages.get(0).getTestMessageFields().isEmpty() &&
				testMessages.get(0).getTestMessageFields().size() == 6 &&
				testMessages.get(0).getTestMessageFields().get(0).getStrValue().equals("a1"));
	}
	
	@Test
	public void testRecordTypeRetrive() throws Exception {
		RecordTypeEntity recordTypeEntity = serviceAggregator.getRecordTypeRepository().findOne(10000000);		
		Assert.assertTrue("Can't read record types.", recordTypeEntity != null);		
		Assert.assertTrue("Can't read record type fields.",
				recordTypeEntity.getRecordTypeFields().get(0).getFieldName().equals("F11"));
	}
	
	@Test
	public void testSessionsRepository(){
		SessionEntity session = serviceAggregator.getSessionsRepository().findOneByRecordTypeWhichCanInitiateSession(10000000);		
		Assert.assertTrue("Can't find session by record type id.", session != null && session.getId() == 1);
	}
	
	@Test
	public void testSessionRecordTypeKeysRepository(){
		SessionRecordTypeKeysEntity sessionRecordTypeKeysEntity = serviceAggregator.getSessionReportTypeKeysRepository().
				findOneBySessionIdAndRecordTypeId(1, 10000000);		
		Assert.assertTrue("Can't find session record type keys info by session id and record type id.", 
				sessionRecordTypeKeysEntity != null && sessionRecordTypeKeysEntity.getId() == 1);
	}
}
