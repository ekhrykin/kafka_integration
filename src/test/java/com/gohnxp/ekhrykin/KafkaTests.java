package com.gohnxp.ekhrykin;

import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.gmail.gohnxp.KafkaIntegration;
import com.gmail.gohnxp.config.SpringBootKafkaConsumer;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStore;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStoreList;
import com.gmail.gohnxp.domain.flatbuffers.RecordList;
import com.gmail.gohnxp.domain.flatbuffers.ValueStore;
import com.gmail.gohnxp.service.KvStoreService;
import com.google.flatbuffers.FlatBufferBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = KafkaIntegration.class)
@WebAppConfiguration
public class KafkaTests {

	private final static Logger LOGGER = LoggerFactory.getLogger(KafkaTests.class);

	@Autowired
	SpringBootKafkaConsumer kafkaConsumer;
	
	@Autowired
	KvStoreService kvStoreService;
	
	private final int KAFKA_PRODUCER_RUNNING_NUM = 33, 
					KEYVALUEPAIR_INT_VALUESTORELIST_ID = 22,
					KEYVALUEPAIR_INT_VALUESTORE_KEY = 11,
					VALUESTORE_INT_VALUE = 987;
	private final boolean VALUESTORE_BOOL_VALUE = true;
	
	@Test
	public void testSendAndReceive() throws Exception {
		
		byte[] msg1 = createMsg1();
		sendSync(msg1);
		
		Thread.sleep(1000);
		
		byte[] dbValue = kvStoreService.read("TestKey".getBytes());
		
		Assert.assertTrue("Messages was not sended or received.", dbValue != null && dbValue.length > 0);
		
		ByteBuffer bb = ByteBuffer.wrap(dbValue);
		
		RecordList recordList = RecordList.getRootAsRecordList(bb);
		
		Assert.assertTrue("KAFKA_PRODUCER_RUNNING_NUM not setted.", 
				recordList != null && recordList.kafkaProducerRunningNum() == KAFKA_PRODUCER_RUNNING_NUM);
		
		Assert.assertTrue("KEYVALUEPAIR_INT_VALUESTORELIST_ID not setted.", 
				recordList != null && recordList.list(0).id() == KEYVALUEPAIR_INT_VALUESTORELIST_ID);
		
		Assert.assertTrue("KEYVALUEPAIR_INT_VALUESTORE_KEY not setted.", 
				recordList != null && recordList.list(0).srcMem(0).Key() == KEYVALUEPAIR_INT_VALUESTORE_KEY);
		
		Assert.assertTrue("VALUESTORE_BOOL_VALUE not setted.", 
				recordList != null && recordList.list(0).srcMem(0).Value().boolValue() == VALUESTORE_BOOL_VALUE);
		
		Assert.assertTrue("VALUESTORE_INT_VALUE not setted.", 
				recordList != null && recordList.list(0).srcMem(0).Value().intValue() == VALUESTORE_INT_VALUE);
	}
	
	private byte[] createMsg1(){

    	FlatBufferBuilder builder = new FlatBufferBuilder();
    	
    	ValueStore.startValueStore(builder);
    	ValueStore.addBoolValue(builder, VALUESTORE_BOOL_VALUE);
    	ValueStore.addIntValue(builder, VALUESTORE_INT_VALUE);
    	int valueStoreOffset = ValueStore.endValueStore(builder);
    	
    	//===========================
    	
    	KeyValuePair_int_ValueStore.startKeyValuePair_int_ValueStore(builder);
    	KeyValuePair_int_ValueStore.addKey(builder, KEYVALUEPAIR_INT_VALUESTORE_KEY);
    	KeyValuePair_int_ValueStore.addValue(builder, valueStoreOffset);
    	int intValueStoreOffset = KeyValuePair_int_ValueStore.endKeyValuePair_int_ValueStore(builder);
    	
    	//===========================
    	
    	int[] srcMems = {intValueStoreOffset};
    	int srcMemVectorOffset = KeyValuePair_int_ValueStoreList.createSrcMemVector(builder, srcMems);

    	KeyValuePair_int_ValueStoreList.startKeyValuePair_int_ValueStoreList(builder);
    	KeyValuePair_int_ValueStoreList.addId(builder, KEYVALUEPAIR_INT_VALUESTORELIST_ID);    	
    	KeyValuePair_int_ValueStoreList.addSrcMem(builder, srcMemVectorOffset);
    	int intValueStoreListOffset = KeyValuePair_int_ValueStoreList.endKeyValuePair_int_ValueStoreList(builder);
    	
    	//===========================

    	int[] list = {intValueStoreListOffset};
    	int listOffset = RecordList.createListVector(builder, list);
    	
    	RecordList.startRecordList(builder);
    	RecordList.addKafkaProducerRunningNum(builder, KAFKA_PRODUCER_RUNNING_NUM);    	
    	RecordList.addList(builder, listOffset);
    	int recordListOffset = RecordList.endRecordList(builder);
    	
    	//===========================

    	builder.finish(recordListOffset);
    	
    	return builder.sizedByteArray();
	}
	
	@Value("${brokerList}")
    private String brokerList;
 
    @Value("${sync}")
    private String sync;
 
    @Value("${topic}")
    private String topic;
    
    @Value("${kafkaBinDirPath}")
    private String kafkaBinDirPath;
    
    private Producer<String, byte[]> producer;

    @Value("${upscaleDbFileName}")
    private String upscaleDbFileName;
    
    @Value("${upscaleDbNumber}")
    private short upscaleDbNumber;

	@Before
    public void initIt() {
				
        Properties kafkaProps = new Properties();
 
        kafkaProps.put("bootstrap.servers", brokerList);
 
        kafkaProps.put("key.serializer", 
            "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", 
            "org.apache.kafka.common.serialization.ByteArraySerializer");
        kafkaProps.put("acks", "1");
 
        kafkaProps.put("retries", "1");
        kafkaProps.put("linger.ms", 5);
 
        producer = new KafkaProducer<>(kafkaProps);
    }
	
	@After
    public void preDestroy() {
		producer.close();
		LOGGER.info("Kafka producer was closed.");
	}
 
    public void send(byte[] value) throws ExecutionException, 
            InterruptedException {
        if ("sync".equalsIgnoreCase(sync)) {
            sendSync(value);
        } else {
            sendAsync(value);
        }
    }
 
    private void sendSync(byte[] value) throws ExecutionException,
            InterruptedException {
        ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, value);
        RecordMetadata result = producer.send(record).get();
        LOGGER.debug("result == " + result);
    }
 
    private void sendAsync(byte[] value) {
        ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, value);
 
        producer.send(record, (RecordMetadata recordMetadata, Exception e) -> {
            if (e != null) {
                e.printStackTrace();
            }
        });
    }
}
