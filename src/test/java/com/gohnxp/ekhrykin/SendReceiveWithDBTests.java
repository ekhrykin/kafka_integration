package com.gohnxp.ekhrykin;

import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.gmail.gohnxp.KafkaIntegration;
import com.gmail.gohnxp.config.SpringBootKafkaConsumer;
import com.gmail.gohnxp.domain.TestMessageEntity;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStoreList;
import com.gmail.gohnxp.repository.TestMessagesRepository;
import com.gmail.gohnxp.service.KvStoreService;
import com.gmail.gohnxp.service.TestMessageToFlatBufferConverter;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = KafkaIntegration.class)
@WebAppConfiguration
public class SendReceiveWithDBTests {

	private final static Logger LOGGER = LoggerFactory.getLogger(SendReceiveWithDBTests.class);

	@Autowired
	SpringBootKafkaConsumer kafkaConsumer;
	
	@Autowired
	KvStoreService kvStoreService;
	
	@Autowired
	TestMessagesRepository testMessagesRepository;
	
	@Autowired
	TestMessageToFlatBufferConverter messageToFlatBufferConverter;
	
	private final int MSG_R1_ID = 1;
	
	@Test
	public void testSendAndReceive() throws Exception {
		
		TestMessageEntity msgR1 = testMessagesRepository.findOne(MSG_R1_ID);
		
		byte[] msgR1Bytes = messageToFlatBufferConverter.convert(msgR1);
		sendSync(msgR1Bytes);
		
		Thread.sleep(1000);
		
		byte[] dbValue = kvStoreService.read("TestKey".getBytes());
		
		Assert.assertTrue("Messages was not sended or received.", dbValue != null && dbValue.length > 0);
		
		ByteBuffer bb = ByteBuffer.wrap(dbValue);
		
		KeyValuePair_int_ValueStoreList fbMsg = KeyValuePair_int_ValueStoreList.getRootAsKeyValuePair_int_ValueStoreList(bb);
		
		Assert.assertTrue("Message ID not setted.", 
				fbMsg != null && fbMsg.id() == msgR1.getRecordTypeId());
		
		Assert.assertTrue("Field key not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Key() == msgR1.getTestMessageFields().get(0).getFieldId());
		
		Assert.assertTrue("Field value not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Value().strValue().equals(msgR1.getTestMessageFields().get(0).getStrValue()));
	}
	
	@Value("${brokerList}")
    private String brokerList;
 
    @Value("${sync}")
    private String sync;
 
    @Value("${topic}")
    private String topic;
    
    @Value("${kafkaBinDirPath}")
    private String kafkaBinDirPath;
    
    private Producer<String, byte[]> producer;

    @Value("${upscaleDbFileName}")
    private String upscaleDbFileName;
    
    @Value("${upscaleDbNumber}")
    private short upscaleDbNumber;

	@Before
    public void initIt() {
				
        Properties kafkaProps = new Properties();
 
        kafkaProps.put("bootstrap.servers", brokerList);
 
        kafkaProps.put("key.serializer", 
            "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", 
            "org.apache.kafka.common.serialization.ByteArraySerializer");
        kafkaProps.put("acks", "1");
 
        kafkaProps.put("retries", "1");
        kafkaProps.put("linger.ms", 5);
 
        producer = new KafkaProducer<>(kafkaProps);
    }
	
	@After
    public void preDestroy() {
		producer.close();
		LOGGER.info("Kafka producer was closed.");
	}
 
    public void send(byte[] value) throws ExecutionException, 
            InterruptedException {
        if ("sync".equalsIgnoreCase(sync)) {
            sendSync(value);
        } else {
            sendAsync(value);
        }
    }
 
    private void sendSync(byte[] value) throws ExecutionException,
            InterruptedException {
        ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, value);
        RecordMetadata result = producer.send(record).get();
        LOGGER.debug("result == " + result);
    }
 
    private void sendAsync(byte[] value) {
        ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, value);
 
        producer.send(record, (RecordMetadata recordMetadata, Exception e) -> {
            if (e != null) {
                e.printStackTrace();
            }
        });
    }
}
