package com.gohnxp.ekhrykin;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.gohnxp.KafkaIntegration;
import com.gmail.gohnxp.config.SpringBootKafkaConsumer;
import com.gmail.gohnxp.domain.TestMessageEntity;
import com.gmail.gohnxp.domain.flatbuffers.KeyValuePair_int_ValueStoreList;
import com.gmail.gohnxp.repository.TestMessagesRepository;
import com.gmail.gohnxp.service.ConsumerLoop;
import com.gmail.gohnxp.service.FbStatObjectUtils;
import com.gmail.gohnxp.service.KvStoreService;
import com.gmail.gohnxp.service.ServiceAggregator;
import com.gmail.gohnxp.service.TestMessageToFlatBufferConverter;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = KafkaIntegration.class)
@WebAppConfiguration
public class SessionAggTests {

	private final static Logger LOGGER = LoggerFactory.getLogger(SessionAggTests.class);

	@Autowired
	SpringBootKafkaConsumer kafkaConsumer;
	
	@Autowired
	KvStoreService kvStoreService;
	
	@Autowired
	TestMessagesRepository testMessagesRepository;
	
	@Autowired
	TestMessageToFlatBufferConverter messageToFlatBufferConverter;
	
	@Autowired
	private ServiceAggregator serviceAggregator;
	
	@Value("${kafkaConsumerId}")
	private int kafkaConsumerId;
	
	@Value("${kafkaConsumersGroup}")
	private String kafkaConsumersGroup;
	
	private final int MSG_R1_ID = 1;
	private final int MSG_R2_ID = 2;
	private final int MSG_R3_ID = 3;
	
	private ConsumerLoop loop;
	
	@Test
	@Transactional
	public void testSendAndReceive() throws Exception {
		
		loop = new ConsumerLoop(kafkaConsumerId, 
				kafkaConsumersGroup,
				Arrays.asList(topic),
				brokerList, 
				serviceAggregator);
		
		testFirstMsg();
		testSecondMsg();
		testThirdMsg();
		testThourthMsg();
	}
	
	private void testThirdMsg() throws Exception{
		
		TestMessageEntity msgR3 = testMessagesRepository.findOne(MSG_R3_ID);
		byte[] msgR3Bytes = messageToFlatBufferConverter.convert(msgR3);
		
		loop.processEventMsg(msgR3Bytes);
		
		byte[] eventObjCloneKey = ("a1a2\0" + 3).getBytes();
		byte[] eventObjCloneValue = null;
		try{
			eventObjCloneValue = kvStoreService.read(eventObjCloneKey);
		}catch(Exception e){
			//this is right logic
		}		
		Assert.assertTrue("eventObjClone value must not exists.", eventObjCloneValue == null);

		byte[] mergeKey = ("a1a2" + FbStatObjectUtils.VALUE_PART_SEPARATOR + 0).getBytes();
		ByteBuffer bbStat = ByteBuffer.wrap(kvStoreService.read(mergeKey));		
		KeyValuePair_int_ValueStoreList fbStatMsg = KeyValuePair_int_ValueStoreList.
				getRootAsKeyValuePair_int_ValueStoreList(bbStat);	
		Assert.assertTrue("fbStatMsg value must not be changed after third msg.",
				fbStatMsg != null && fbStatMsg.srcMem(1).Value().intValue() == 2);
	}
	
	private void testThourthMsg() throws Exception{
		
		TestMessageEntity msgR2 = testMessagesRepository.findOne(MSG_R2_ID);		
		byte[] msgR2Bytes = messageToFlatBufferConverter.convert(msgR2);
		
		loop.processEventMsg(msgR2Bytes);
		
		byte[] eventObjCloneKey = ("a1a2\0" + 3).getBytes();		
		byte[] eventObjCloneValue = kvStoreService.read(eventObjCloneKey);		
		Assert.assertTrue("eventObjClone value was not wrote.", eventObjCloneValue != null && eventObjCloneValue.length > 0);
		ByteBuffer bb = ByteBuffer.wrap(eventObjCloneValue);		
		KeyValuePair_int_ValueStoreList fbMsg = KeyValuePair_int_ValueStoreList.
				getRootAsKeyValuePair_int_ValueStoreList(bb);		
		Assert.assertTrue("eventObjClone ID not setted.", 
				fbMsg != null && fbMsg.id() == msgR2.getRecordTypeId());
		Assert.assertTrue("eventObjClone key not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Key() == msgR2.getTestMessageFields().get(0).getFieldId());		
		Assert.assertTrue("eventObjClone value not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Value().strValue().equals(
						msgR2.getTestMessageFields().get(0).getStrValue()));

		byte[] mergeKey = ("a1a2" + FbStatObjectUtils.VALUE_PART_SEPARATOR + 0).getBytes();		
		byte[] mergeValue = kvStoreService.read(mergeKey);
		Assert.assertTrue("Merged value was not wrote.", mergeValue != null && mergeValue.length > 0);
		ByteBuffer bbStat = ByteBuffer.wrap(mergeValue);		
		KeyValuePair_int_ValueStoreList fbStatMsg = KeyValuePair_int_ValueStoreList.
				getRootAsKeyValuePair_int_ValueStoreList(bbStat);
		TestMessageEntity msgR1 = testMessagesRepository.findOne(MSG_R1_ID);
		Assert.assertTrue("fbStatMsg key not setted.",
				fbStatMsg != null && fbStatMsg.srcMem(0).Value().intValue() == msgR1.getRecordTypeId());		
		Assert.assertTrue("fbStatMsg value not setted.",
				fbStatMsg != null && fbStatMsg.srcMem(1).Value().intValue() == 3);
	}
	
	private void testSecondMsg() throws Exception{
		
		TestMessageEntity msgR2 = testMessagesRepository.findOne(MSG_R2_ID);		
		byte[] msgR2Bytes = messageToFlatBufferConverter.convert(msgR2);
		
		loop.processEventMsg(msgR2Bytes);
		
		byte[] eventObjCloneKey = ("a1a2\0" + 2).getBytes();		
		byte[] eventObjCloneValue = kvStoreService.read(eventObjCloneKey);		
		Assert.assertTrue("eventObjClone value was not wrote.", eventObjCloneValue != null && eventObjCloneValue.length > 0);
		ByteBuffer bb = ByteBuffer.wrap(eventObjCloneValue);		
		KeyValuePair_int_ValueStoreList fbMsg = KeyValuePair_int_ValueStoreList.
				getRootAsKeyValuePair_int_ValueStoreList(bb);		
		Assert.assertTrue("eventObjClone ID not setted.", 
				fbMsg != null && fbMsg.id() == msgR2.getRecordTypeId());
		Assert.assertTrue("eventObjClone key not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Key() == msgR2.getTestMessageFields().get(0).getFieldId());		
		Assert.assertTrue("eventObjClone value not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Value().strValue().equals(
						msgR2.getTestMessageFields().get(0).getStrValue()));

		byte[] mergeKey = ("a1a2" + FbStatObjectUtils.VALUE_PART_SEPARATOR + 0).getBytes();		
		byte[] mergeValue = kvStoreService.read(mergeKey);
		Assert.assertTrue("Merged value was not wrote.", mergeValue != null && mergeValue.length > 0);
		ByteBuffer bbStat = ByteBuffer.wrap(mergeValue);		
		KeyValuePair_int_ValueStoreList fbStatMsg = KeyValuePair_int_ValueStoreList.
				getRootAsKeyValuePair_int_ValueStoreList(bbStat);
		TestMessageEntity msgR1 = testMessagesRepository.findOne(MSG_R1_ID);
		Assert.assertTrue("fbStatMsg key not setted.",
				fbStatMsg != null && fbStatMsg.srcMem(0).Value().intValue() == msgR1.getRecordTypeId());		
		Assert.assertTrue("fbStatMsg value not setted.",
				fbStatMsg != null && fbStatMsg.srcMem(1).Value().intValue() == 2);
	}
	
	private void testFirstMsg() throws Exception{
		
		TestMessageEntity msgR1 = testMessagesRepository.findOne(MSG_R1_ID);
		byte[] msgR1Bytes = messageToFlatBufferConverter.convert(msgR1);
		
		loop.processEventMsg(msgR1Bytes);
		
		byte[] eventObjCloneKey = ("a1a2" + FbStatObjectUtils.VALUE_PART_SEPARATOR + 1).getBytes();		
		byte[] eventObjCloneValue = kvStoreService.read(eventObjCloneKey);		
		Assert.assertTrue("eventObjClone value was not wrote.", eventObjCloneValue != null && eventObjCloneValue.length > 0);
		ByteBuffer bb = ByteBuffer.wrap(eventObjCloneValue);		
		KeyValuePair_int_ValueStoreList fbMsg = KeyValuePair_int_ValueStoreList.
				getRootAsKeyValuePair_int_ValueStoreList(bb);		
		Assert.assertTrue("eventObjClone ID not setted.", 
				fbMsg != null && fbMsg.id() == msgR1.getRecordTypeId());		
		Assert.assertTrue("eventObjClone key not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Key() == msgR1.getTestMessageFields().get(0).getFieldId());		
		Assert.assertTrue("eventObjClone value not setted.", 
				fbMsg != null && fbMsg.srcMem(0).Value().strValue().equals(
						msgR1.getTestMessageFields().get(0).getStrValue()));

		byte[] mergeKey = ("a1a2" + FbStatObjectUtils.VALUE_PART_SEPARATOR + 0).getBytes();		
		byte[] mergeValue = kvStoreService.read(mergeKey);
		Assert.assertTrue("Merged value was not wrote.", mergeValue != null && mergeValue.length > 0);
		ByteBuffer bbStat = ByteBuffer.wrap(mergeValue);		
		KeyValuePair_int_ValueStoreList fbStatMsg = KeyValuePair_int_ValueStoreList.
				getRootAsKeyValuePair_int_ValueStoreList(bbStat);
		Assert.assertTrue("fbStatMsg key not setted.",
				fbStatMsg != null && fbStatMsg.srcMem(0).Value().intValue() == msgR1.getRecordTypeId());		
		Assert.assertTrue("fbStatMsg value not setted.", 
				fbStatMsg != null && fbStatMsg.srcMem(1).Value().intValue() == 1);

		byte[] purgeKey = "b1".getBytes();
		byte[] purgeValue = kvStoreService.read(purgeKey);
		Assert.assertTrue("Purged value was not wrote.", purgeValue != null && purgeValue.length > 0);
		Assert.assertTrue("Purged value in db is wrong.", 
				Arrays.equals(("a1" + FbStatObjectUtils.VALUE_PART_SEPARATOR + "a2").getBytes(), purgeValue));
	}
	
	@Value("${brokerList}")
    private String brokerList;
 
    @Value("${sync}")
    private String sync;
 
    @Value("${topic}")
    private String topic;
    
    @Value("${kafkaBinDirPath}")
    private String kafkaBinDirPath;
    
    private Producer<String, byte[]> producer;

    @Value("${upscaleDbFileName}")
    private String upscaleDbFileName;
    
    @Value("${upscaleDbNumber}")
    private short upscaleDbNumber;

	@Before
    public void initIt() {
				
        Properties kafkaProps = new Properties();
 
        kafkaProps.put("bootstrap.servers", brokerList);
 
        kafkaProps.put("key.serializer", 
            "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", 
            "org.apache.kafka.common.serialization.ByteArraySerializer");
        kafkaProps.put("acks", "1");
 
        kafkaProps.put("retries", "1");
        kafkaProps.put("linger.ms", 5);
 
        producer = new KafkaProducer<>(kafkaProps);
    }
	
	@After
    public void preDestroy() {
		producer.close();
		LOGGER.info("Kafka producer was closed.");
	}
 
    public void send(byte[] value) throws ExecutionException, 
            InterruptedException {
        if ("sync".equalsIgnoreCase(sync)) {
            sendSync(value);
        } else {
            sendAsync(value);
        }
    }
 
    private void sendSync(byte[] value) throws ExecutionException,
            InterruptedException {
        ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, value);
        RecordMetadata result = producer.send(record).get();
        LOGGER.debug("result == " + result);
    }
 
    private void sendAsync(byte[] value) {
        ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, value);
 
        producer.send(record, (RecordMetadata recordMetadata, Exception e) -> {
            if (e != null) {
                e.printStackTrace();
            }
        });
    }
}
